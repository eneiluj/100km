let currentAjaxRequest = null;
let currentDepartementLayer = null;

var theMap = L.map('map').setView([46.415,1.461], 6);
var circle = null;
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>',
	maxZoom: 19,
}).addTo(theMap);

L.Control.geocoder({
	defaultMarkGeocode: false
}).on('markgeocode', function(e) {
  var center = e.geocode.center;
  if(circle != null) circle.removeFrom(theMap);
  circle = L.circle([center.lat, center.lng], {radius: 100000});
  circle.addTo(theMap);
  getDepartement(center.lat, center.lng);
}).addTo(theMap);

theMap.on('click', function(e) {
  if(circle != null) {
    circle.removeFrom(theMap);
  }
  circle = L.circle([e.latlng.lat, e.latlng.lng], {radius: 100000});
  circle.addTo(theMap);
  getDepartement(e.latlng.lat, e.latlng.lng);
});

function getDepartement(lat, lng) {
  // remove departement from map
  if (theMap.hasLayer(currentDepartementLayer)) {
    theMap.removeLayer(currentDepartementLayer);
  }
  const query = 'is_in('+lat+', '+lng+');rel(pivot)[boundary=administrative][admin_level=6];out geom;';
  let overpassQueryUrl = 'https://overpass-api.de/api/interpreter?data=' +
                         encodeURIComponent(query);
  // cancel current running ajax request if necessary
  if (currentAjaxRequest !== null) {
    currentAjaxRequest.abort();
    currentAjaxRequest = null;
  }
  // cursor style
  $('.leaflet-container').css('cursor', 'wait');
  $('.leaflet-interactive').css('cursor', 'wait');
  currentAjaxRequest = $.ajax({
    type: 'GET',
    url: overpassQueryUrl,
    data: null,
    async: true,
  }).done(function(response) {
    processOverpassData(response)
  }).always(function() {
    currentAjaxRequest = null;
    $('.leaflet-container').css('cursor', 'grab');
  $('.leaflet-interactive').css('cursor', 'pointer');
  });
}

function processOverpassData(data) {
  var geojson = osmtogeojson(data);
  currentDepartementLayer = L.geoJSON(geojson, {
    pointToLayer: function (geoJsonPoint, latlng) {
      return null;
    }
  }).addTo(theMap);
}
