# 100km

Projet fait en 10 minutes sous licence AGPL

## À quoi ça sert ?
Tracer un cercle de 100km de rayon autour d'un point donné

## Limites connues
- Ça ne prend pas en compte les frontières des états
- Ça n'affiche pas les limitations de départements
- Ça ne prend pas en compte les règles locales (plages…)

## Possibilité
- Faire une recherche avec le champ de recherche
- Avoir une zone de 100km au *click*

## Technologie
- [OpenStreetMap](https://www.openstreetmap.org/)
- [Nominatim](https://nominatim.openstreetmap.org/)
- [Leaflet](https://leafletjs.com/)
- [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
